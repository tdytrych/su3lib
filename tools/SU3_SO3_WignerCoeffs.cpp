#include <iomanip>
#include <iostream>

#include <su3.h>

int main(int argc, char** argv) {
   su3::init(250);
   int lm1, mu1, lm2, mu2, lm3, mu3;
   int L1sel, L2sel, L3sel;

   std::cout << "SU(3) Wigner coefficients in physical basis: <(lm1 mu1) k1 L1; (lm2 mu3) k2 L2|| "
                "(lm3 mu3) k3 L3>rho."
             << std::endl;

   std::cout << "Enter (lm1 mu1) " << std::endl;
   std::cin >> lm1 >> mu1;
   std::cout << "Enter L1 ... -1 to include all values" << std::endl;
   std::cin >> L1sel;
   if (L1sel != -1 && su3::kmax(lm1, mu1, L1sel) == 0) {
      std::cerr << "L=" << L1sel << " does not belong to (" << lm1 << " " << mu1 << ") irrep."
                << std::endl;
      return EXIT_FAILURE;
   }

   std::cout << "Enter (lm2 mu2) " << std::endl;
   std::cin >> lm2 >> mu2;
   std::cout << "Enter L2 .... -1 to include all values" << std::endl;
   std::cin >> L2sel;
   if (L2sel != -1 && su3::kmax(lm2, mu2, L2sel) == 0) {
      std::cerr << "L=" << L2sel << " does not belong to (" << lm2 << " " << mu2 << ") irrep."
                << std::endl;
      return EXIT_FAILURE;
   }

   std::cout << "Enter (lm3 mu3) " << std::endl;
   std::cin >> lm3 >> mu3;
   std::cout << "Enter L3 .... -1 to include all values" << std::endl;
   std::cin >> L3sel;
   if (L3sel != -1 && su3::kmax(lm3, mu3, L3sel) == 0) {
      std::cerr << "L=" << L3sel << " does not belong to (" << lm3 << " " << mu3 << ") irrep."
                << std::endl;
      return EXIT_FAILURE;
   }

   if (L1sel >= 0 && L2sel >= 0 && L3sel >= 0) {
      if (L3sel < std::abs(L1sel - L2sel) || L3sel > std::abs(L1sel + L2sel)) {
         std::cerr << "L1=" << L1sel << " and L2=" << L2sel << " can not be coupled to L3=" << L3sel
                   << std::endl;
         return EXIT_FAILURE;
      }
   }

   int rhomax = su3::mult(lm1, mu1, lm2, mu2, lm3, mu3);
   if (!rhomax) {
      std::cerr << "(" << lm1 << " " << mu1 << ") and (" << lm2 << " " << mu2
                << ") does not couple to (" << lm3 << " " << mu3 << ")" << std::endl;
      return EXIT_FAILURE;
   }

   int L1min, L1max, L2min, L2max, L3min, L3max;

   L1min = L1max = L1sel;
   L2min = L2max = L2sel;
   L3min = L3max = L3sel;

   if (L1sel == -1) {
      L1min = 0;
      L1max = lm1 + mu1;
   }

   if (L2sel == -1) {
      L2min = 0;
      L2max = lm2 + mu2;
   }

   if (L3sel == -1) {
      L3min = 0;
      L3max = lm3 + mu3;
   }

   bool first = true;
   std::cout << std::setprecision(8);
   std::cout << "<(lm1 mu1) k1 L1; (lm2 mu2) k2 L2 || (lm3 mu3) k3 L3> rho = 1 ... " << rhomax
             << std::endl;
   std::vector<double> su3cgs;
//   std::vector<double> d;
   su3cgs.reserve(std::max(lm1, mu1) * std::max(lm2, mu2) * std::max(lm3, mu3));
   for (int L1 = L1min; L1 <= L1max; ++L1) {
      int k1max = su3::kmax(lm1, mu1, L1);
      if (!k1max) {
         continue;
      }
      for (int L2 = L2min; L2 <= L2max; ++L2) {
         int k2max = su3::kmax(lm2, mu2, L2);
         if (!k2max) {
            continue;
         }
         for (int L3 = L3min; L3 <= L3max; ++L3) {
            int k3max = su3::kmax(lm3, mu3, L3);
            if (!k3max) {
               continue;
            }
            if (L3 < std::abs(L1 - L2) || L3 > (L1 + L2)) {
               continue;
            }

            su3::wu3r3w(lm1, mu1, lm2, mu2, lm3, mu3, L1, L2, L3, k3max, k2max, k1max, rhomax,
                        su3cgs);

            size_t index = 0;
            for (int k3 = 0; k3 < k3max; k3++)  // for all values of k3
            {
               for (int k2 = 0; k2 < k2max; k2++)  // for all values of k2
               {
                  for (int k1 = 0; k1 < k1max; k1++)  // for all values of k1
                  {
                     if (first) {
                        std::cout << std::setw(4) << lm1 << "  " << mu1;
                        std::cout << std::setw(6) << k1 + 1 << " " << L1;
                        std::cout << std::setw(6) << lm2 << "  " << mu2;
                        std::cout << std::setw(6) << k2 + 1 << " " << L2;
                        std::cout << std::setw(8) << lm3 << "  " << mu3;
                        std::cout << std::setw(6) << k3 + 1 << " " << L3;
                        first = false;
                     } else {
                        std::cout << std::setw(13) << k1 + 1 << " " << L1;
                        std::cout << std::setw(15) << k2 + 1 << " " << L2;
                        std::cout << std::setw(17) << k3 + 1 << " " << L3;
                     }
                     for (int irho = 0; irho < rhomax; irho++)  // for all possible multiplicities
                     {
//                        d.push_back(su3cgs[index]);
                        std::cout << std::fixed << std::setw(13) << std::setfill(' ')
                                  << su3cgs[index++];
                     }
                     std::cout << std::endl;
                  }
               }
            }
         }
      }
   }
/*   
   // => compute \sum c^2 to check normalization
   if (L1sel == -1 && L2sel == -1)
   {
      double result = 0;
      for (auto c : d) {
         result += c * c;
      }
      std::cout << "sum c^2 = " << result << std::endl;
   }
*/   
   su3::finalize();
}
