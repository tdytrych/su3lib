#include <cassert>
#include <iomanip>
#include <iostream>
#include <vector>

#include <su3.h>

int main(int argc, char** argv) {
   su3::init(250);

   int lm1, mu1, lm2, mu2, lm3, mu3, lm4, mu4, lm12, mu12, lm34, mu34, lm13, mu13, lm24, mu24, lm,
       mu;

   std::cout << "Computation of U9 recoupling coefficients:" << std::endl;
   std::cout << "(lm1 mu1)   (lm2 mu2)   (lm12 mu12) rho12" << std::endl;
   std::cout << "(lm3 mu3)   (lm4 mu4)   (lm34 mu34) rho34" << std::endl;
   std::cout << "(lm13 mu13) (lm24 mu24) (lm mu)     rho1324" << std::endl;
   std::cout << "   rho13       rho24    rho1234" << std::endl << std::endl;

   std::cout << "Enter (lm1 mu1) " << std::endl;
   std::cin >> lm1 >> mu1;
   std::cout << "Enter (lm2 mu2) " << std::endl;
   std::cin >> lm2 >> mu2;
   std::cout << "Enter (lm12 mu12) " << std::endl;
   std::cin >> lm12 >> mu12;

   std::cout << "Enter (lm3 mu3) " << std::endl;
   std::cin >> lm3 >> mu3;
   std::cout << "Enter (lm4 mu4) " << std::endl;
   std::cin >> lm4 >> mu4;
   std::cout << "Enter (lm34 mu34) " << std::endl;
   std::cin >> lm34 >> mu34;

   std::cout << "Enter (lm13 mu13) " << std::endl;
   std::cin >> lm13 >> mu13;
   std::cout << "Enter (lm24 mu24) " << std::endl;
   std::cin >> lm24 >> mu24;
   std::cout << "Enter (lm mu) " << std::endl;
   std::cin >> lm >> mu;

   size_t nsu39lm;

   int rho12max = su3::mult(lm1, mu1, lm2, mu2, lm12, mu12);
   int rho34max = su3::mult(lm3, mu3, lm4, mu4, lm34, mu34);
   int rho13max = su3::mult(lm1, mu1, lm3, mu3, lm13, mu13);
   int rho24max = su3::mult(lm2, mu2, lm4, mu4, lm24, mu24);
   int rho1234max = su3::mult(lm12, mu12, lm34, mu34, lm, mu);
   int rho1324max = su3::mult(lm13, mu13, lm24, mu24, lm, mu);

   bool isOk = true;

   if (!rho12max) {
      std::cerr << "(" << lm1 << " " << mu1 << ") x (" << lm2 << " " << mu2
                << ") does not couple to ";
      std::cerr << "(" << lm12 << " " << mu12 << ")" << std::endl;
      isOk = false;
   }
   if (!rho34max) {
      std::cerr << "(" << lm3 << " " << mu3 << ") x (" << lm4 << " " << mu4
                << ") does not couple to ";
      std::cerr << "(" << lm34 << " " << mu34 << ")" << std::endl;
      isOk = false;
   }
   if (!rho13max) {
      std::cerr << "(" << lm1 << " " << mu1 << ") x (" << lm3 << " " << mu3
                << ") does not couple to ";
      std::cerr << "(" << lm13 << " " << mu13 << ")" << std::endl;
      isOk = false;
   }
   if (!rho24max) {
      std::cerr << "(" << lm2 << " " << mu2 << ") x (" << lm4 << " " << mu4
                << ") does not couple to ";
      std::cerr << "(" << lm24 << " " << mu24 << ")" << std::endl;
      isOk = false;
   }

   if (!rho1234max) {
      std::cerr << "(" << lm12 << " " << mu12 << ") x (" << lm34 << " " << mu34
                << ") does not couple to ";
      std::cerr << "(" << lm << " " << mu << ")" << std::endl;
      isOk = false;
   }
   if (!rho1324max) {
      std::cerr << "(" << lm13 << " " << mu13 << ") x (" << lm24 << " " << mu24
                << ") does not couple to ";
      std::cerr << "(" << lm << " " << mu << ")" << std::endl;
      isOk = false;
   }

   if (!isOk) {
      return EXIT_FAILURE;
   }


   std::vector<double> su39lm;
   su3::wu39lm(lm1, mu1, lm2, mu2, lm12, mu12, lm3, mu3, lm4, mu4, lm34, mu34, lm13, mu13, lm24,
               mu24, lm, mu, su39lm);

   nsu39lm = rho12max * rho34max * rho13max * rho24max * rho1234max * rho1324max;
   assert(su39lm.size() == nsu39lm);

   std::cout << "(" << lm1 << " " << mu1 << ")  ";
   std::cout << "(" << lm2 << " " << mu2 << ")  ";
   std::cout << "(" << lm12 << " " << mu12 << ")   rho12\n";

   std::cout << "(" << lm3 << " " << mu3 << ")  ";
   std::cout << "(" << lm4 << " " << mu4 << ")  ";
   std::cout << "(" << lm34 << " " << mu34 << ")   rho34\n";

   std::cout << "(" << lm13 << " " << mu13 << ")  ";
   std::cout << "(" << lm24 << " " << mu24 << ")  ";
   std::cout << "(" << lm << " " << mu << ")   rho1324\n";
   std::cout << "rho13  rho24  rho1234" << std::endl << std::endl;

   int n1 = rho12max;
   int n2 = n1 * rho34max;
   int n3 = n2 * rho1234max;
   int n4 = n3 * rho13max;
   int n5 = n4 * rho24max;

   std::cout << std::setprecision(8);
   std::cout << std::setw(8) << "rho1324" << std::setw(8) << "rho24" << std::setw(8) << "rho13";
   std::cout << std::setw(8) << "rho1234" << std::setw(8) << "rho34" << std::setw(8) << "rho12";
   std::cout << std::endl;

   for (int rho1324 = 0, index = 0; rho1324 < rho1324max; ++rho1324) {
      for (int rho24 = 0; rho24 < rho24max; ++rho24) {
         for (int rho13 = 0; rho13 < rho13max; ++rho13) {
            for (int rho1234 = 0; rho1234 < rho1234max; ++rho1234) {
               for (int rho34 = 0; rho34 < rho34max; ++rho34) {
                  for (int rho12 = 0; rho12 < rho12max; ++rho12, ++index) {
                     assert(rho12 + rho34 * n1 + rho1234 * n2 + rho13 * n3 + rho24 * n4 +
                                rho1324 * n5 ==
                            index);
                     std::cout << std::setw(5) << rho1324 << std::setw(8) << rho24 << std::setw(8)
                               << rho13;
                     std::cout << std::setw(8) << rho1234 << std::setw(8) << rho34 << std::setw(8)
                               << rho12 << std::setw(5);
                     std::cout << std::fixed << std::setw(15) << su39lm[index] << std::endl;
                  }
               }
            }
         }
      }
   }

   su3::finalize();

   return EXIT_SUCCESS;
}
