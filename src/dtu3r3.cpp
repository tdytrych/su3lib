#include <cassert>
#include <cmath>

#include <boost/multiprecision/cpp_bin_float.hpp>
#include <boost/multiprecision/float128.hpp>

#include <su3.h>

#ifdef DTU3R3_CACHE
#include <cstdint>
#include <unordered_map>
#endif

namespace su3 {

#ifdef DTU3R3_CACHE

static std::unordered_map<uint64_t, double>* cache;
#pragma omp threadprivate(cache)

void dtu3r3_cache_init() { cache = new std::unordered_map<uint64_t, double>{}; }

void dtu3r3_cache_finalize() { delete cache; }

#endif

static double dtu3r3_middle(int lm, int mu, int Lam_2, int MLam_2, int Kbar, int L, int Mbar, int p,
                            int q, int r) {
   const int precision_measure = lm + mu + L;

   // limits are set to ensure difference from exact value <= 5.0e-15
   if (precision_measure < 18) {
      return dtu3r3_internal<double>(lm, mu, Lam_2, MLam_2, Kbar, L, Mbar, p, q, r);
   } else if (precision_measure < 30) {
#ifdef HAVE_80BIT_LONG_DOUBLE
      return dtu3r3_internal<long double>(lm, mu, Lam_2, MLam_2, Kbar, L, Mbar, p, q, r);
#else
      return dtu3r3_internal<boost::multiprecision::cpp_bin_float_double_extended>
         (lm, mu, Lam_2, MLam_2, Kbar, L, Mbar, p, q, r);
#endif
   } else if (precision_measure < 97) {
#ifdef HAVE_FLOAT128
      return dtu3r3_internal<boost::multiprecision::float128>
         (lm, mu, Lam_2, MLam_2, Kbar, L, Mbar, p, q, r);
#else
      return dtu3r3_internal<boost::multiprecision::cpp_bin_float_quad>
         (lm, mu, Lam_2, MLam_2, Kbar, L, Mbar, p, q, r);
#endif
   }
   return su3::dtu3r3_internal<boost::multiprecision::cpp_bin_float_oct>
      (lm, mu, Lam_2, MLam_2, Kbar, L, Mbar, p, q, r);
}

double dtu3r3(int I, int J, int lm, int mu, int eps, int Lam_2, int MLam_2, int K, int L, int M) {
   if (I != 1) {
      std::swap(lm, mu);
      eps = -eps;
      MLam_2 = -MLam_2;
   }

   assert((2 * (lm - mu) + 3 * Lam_2 - eps) % 6 == 0);
   const int p = (2 * (lm - mu) + 3 * Lam_2 - eps) / 6;

   const int q = mu + p - Lam_2;

   assert((Lam_2 + MLam_2) % 2 == 0);
   const int r = (Lam_2 + MLam_2) / 2;

   int iph = 0;
   if (K < 0) iph += p + q + L + M;
   if (M < 0) iph += q + r + L + K;

   const int Kbar = std::abs(K);
   const int Mbar = std::abs(M);

#ifdef DTU3R3_CACHE
   uint64_t key = (uint64_t(lm & 0xFF) << 56) | (uint64_t(mu & 0xFF) << 48) |
                  (uint64_t(eps & 0xFF) << 40) | (uint64_t(Lam_2 & 0xFF) << 32) |
                  (uint64_t(MLam_2 & 0xFF) << 24) | (uint64_t(Kbar & 0xFF) << 16) |
                  (uint64_t(L & 0xFF) << 8) | (uint64_t(Mbar & 0xFF));

   const auto& temp = cache->insert({key, 0.0});
   if (temp.second)
      temp.first->second = dtu3r3_middle(lm, mu, Lam_2, MLam_2, Kbar, L, Mbar, p, q, r);
   double res = temp.first->second;
#else
   double res = dtu3r3_middle(lm, mu, Lam_2, MLam_2, Kbar, L, Mbar, p, q, r);
#endif

   if ((I != J) && (((lm + K) / 2) % 2)) res = -res;

   if ((L - p + iph) % 2)  // from dtu3r3.F, not in eq. (26):
      res = -res;

   return res;
}

}  // namespace su3
