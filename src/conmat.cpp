#include <algorithm>
#include <cassert>
#include <cmath>
#include <iostream>
#include <vector>

#include <su3.h>

namespace su3
{

void Conmat(int I, int J, int lm, int mu, int kmax, int L, double* O) {
   int Kmin = su3::Kmin(I, J, lm, mu, L);
   int eps, LM, M_LM;
   std::tie(eps, LM, M_LM) = GetExtremalStateLabels(I, J, lm, mu);

   double d = dtu3r3(I, J, lm, mu, eps, LM, M_LM, Kmin, L, Kmin);

   // O[0][0]
   O[0] = 1.0 / std::sqrt(dtu3r3(I, J, lm, mu, eps, LM, M_LM, Kmin, L, Kmin));
   if (kmax == 1)  // => we are done
   {
      return;
   }
   // generate the rest of the first row
   int Kj = Kmin + 2;
   for (int j = 1; j < kmax; ++j, Kj += 2) {
      // O[0][j]
      O[j] = O[0] * dtu3r3(I, J, lm, mu, eps, LM, M_LM, Kj, L, Kmin);
   }

   //   return;

   int Ki = Kmin + 2;
   for (int i = 1; i < kmax; ++i) {
      // generate O[i][i]
      size_t ji_index = i;  // ji_index <-- O[0][i]
      double dsum = 0.0;
      for (int j = 0; j < i; ++j) {
         dsum += O[ji_index] * O[ji_index];
         ji_index += kmax;
      }
      assert(kmax * i + i == ji_index);
      size_t ii_index = ji_index;  // ii_index <-- O[i][i]
      // O[i][i]
      O[ii_index] = 1.0 / (std::sqrt(dtu3r3(I, J, lm, mu, eps, LM, M_LM, Ki, L, Ki) - dsum));

      // generate the rest of ith row O[i][i < j < kmax]
      size_t ij_index = ii_index + 1;  // ij_index <-- O[i][i+1]
      Kj = Ki + 2;
      for (int j = i + 1; j < kmax; ++j) {
         size_t ki_index = i;  // ki_index <-- O[k=0][i]
         size_t kj_index = j;  // kj_index <-- O[k=0][j]
         dsum = 0.0;
         for (int k = 0; k < i; ++k) {
            dsum += O[ki_index] * O[kj_index];
            kj_index += kmax;
            ki_index += kmax;
         }
         // O[i][j]
         O[ij_index++] = O[ii_index] * (dtu3r3(I, J, lm, mu, eps, LM, M_LM, Kj, L, Ki) - dsum);
         Kj += 2;
      }
      Ki += 2;
   }

   // generate lower diagonal matrix
   size_t ii_index = kmax + 1;
   size_t i_index = kmax;
   for (int i = 1; i < kmax; ++i) {
      size_t j_index = 0;
      for (int j = 0; j < i; ++j) {
         size_t kj_index = j_index + j;
         size_t ki_index = j_index + i;
         double dsum = 0.0;
         for (int k = j; k < i; ++k) {
            dsum -= O[kj_index] * O[ki_index];
            kj_index += kmax;
            ki_index += kmax;
         }
         O[i_index + j] = O[ii_index] * dsum;
         // Following operation is not documented in Draayer & Akiyama!
         O[j_index + i] *= -O[ii_index];
         j_index += kmax;
      }
      i_index += kmax;
      ii_index += (kmax + 1);
   }
}

} // namespace su3
