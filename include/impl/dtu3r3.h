#ifndef SU3_DTU3R3_H
#define SU3_DTU3R3_H

#include <cassert>
#include <cmath>

#include <boost/multiprecision/number.hpp>

#include "common.h"
#include "../su3_config.h"

namespace su3
{

#ifdef DTU3R3_CACHE
void dtu3r3_cache_init();
void dtu3r3_cache_finalize();
#endif

double dtu3r3(int I, int J, int lm, int mu, int eps, int Lam_2, int MLam_2, int K, int L,
              int M);

// Compute I(p, q, s) using relation (B.2) provided in Bahri, Rowe, and Draayer, Comp. Phys. Comm.
// 159 121(2004)
template <typename T>
T Iv1(int p, int q, int s)
{
   T result = 0.0;

   int beta_min = std::max(0, s - p);
   int beta_max = std::min(q, s);

   int id1 = n_k(q, beta_min);
   int id2 = n_k(p, s - beta_min);

   for (int beta = beta_min; beta <= beta_max; beta++) {
      T temp_beta = bino<T>(id1++) * bino<T>(id2--);

      if (beta % 2) temp_beta = -temp_beta;
      result += temp_beta;
   }
   return result;
}

// For q >= p, apply (B.5) in Bahri, Rowe, and Draayer, Comp. Phys. Comm.  159 121(2004).
// to compute I(p, q, s)
template <typename T>
T relation_B5(int p, int q, int s)
{
   T result = 0.0;

   int beta_min = std::max(0, (int)ceil((s + p - q) / 2.0));
   int beta_max = std::min(p, (int)floor(s / 2.0));

   int id1 = n_k(p, beta_min);
   int id2 = n_k(q - p, s - 2*beta_min);

   for (int beta = beta_min; beta <= beta_max; beta++) {
//    T temp_beta = bino<T>(p, beta) * bino<T>(q - p, s - 2*beta);
//    T temp_beta = bino<T>(id1++) * bino<T>(q - p, s - 2*beta);
      T temp_beta = bino<T>(id1++) * bino<T>(id2);
      id2 -= 2;

      if (beta % 2) temp_beta = -temp_beta;
      result += temp_beta;
   }
// pow(-1.0, s) * result;
   if (s % 2) {
      result = -result;
   }
   return result;
}

// Compute I(p, q, s) using relation (B.5) provided in Bahri, Rowe, and Draayer, Comp. Phys. Comm.
// 159 121(2004) and symmetry property I(p, q, s) = (-)^(s) I(p, q, s)
template <typename T>
T Iv2(int p, int q, int s)
{
   T result;
   if (q >= p) {
      result = relation_B5<T>(p, q, s);
   } else { // q < p => (-)^s I(p, q, s)
      result = relation_B5<T>(q, p, s);
// pow(-1.0, s) * result;
      if (s % 2) {
         result = -result;
      }
   }
   return result;
}

template <typename T>
double dtu3r3_internal(int lm, int mu, int Lam_2, int MLam_2, int Kbar, int L, int Mbar, int p,
                       int q, int r)
{
   if ((Lam_2 + Mbar) % 2 != 0) {
      return 0;
   }
   const int u = (Lam_2 + Mbar) / 2;

   if ((Kbar + lm) % 2 != 0) {
      return 0;
   }
   const int Kbarplm = (Kbar + lm) / 2;

   T C = (2.0 * L + 1) / std::pow(2.0, 2.0 * p);
   T temp = bino<T>(lm, p) * bino<T>(mu, q) * bino<T>(lm + mu + 1, q) * bino<T>(2 * L, L - Kbar) *
            binv<T>(2 * L, L - Mbar) * binv<T>(Lam_2, r) * binv<T>(p + mu + 1, q);

   using boost::multiprecision::sqrt;
   using std::sqrt;
   C *= sqrt(temp);

   // optimizations:
   const int s = p + mu - q;
   assert(s == Lam_2);

   const int sr = s - r;
   const int usr = u + sr;
   const int s2r = 2 * s - r;

   assert((Kbar - lm) % 2 == 0);
   const int Kbarmlm = (Kbar - lm) / 2;

   const int lmmu = lm + mu;
   const int pq = p + q;

   const int LKbar = L - Kbar;
   const int LMbar = L - Mbar;
   const int KbarMbar = Kbar + Mbar;
   const int LpKbar = L + Kbar;

   T res = 0.0;
   for (int gamma = 0; gamma <= p; gamma++) {
      const int k2 = lmmu - gamma;     // 2 * k
      const int kkappa_ = pq - gamma;  // k + kappa_

      // optimizations:
      const int t = p - gamma;
      const int usrt = usr - t;
      const int srt = sr - t;
      const int s2rt = s2r - t;
      const int lmgamma = lm - gamma;

      const int x = lmmu - gamma;
      assert(x == k2);

      const int y = pq - gamma;
      assert(y == kkappa_);

      assert((x + y + KbarMbar) % 2 == 0);
      const int xyKbarMbar = (x + y + KbarMbar) / 2;

      const int xL = x + L;

      const T xL1inv = T{1.0} / (xL + 1.0);

      T temp = bino<T>(p, gamma);

      // S_1( MLam_, Lam_, NLam_, MBar):
      {
         T res = 0.0;

         int alpha_min = std::max(0, sr - t);
         int alpha_max = std::min(s - t, sr);

         int jd1 = n_k(s - t, alpha_min);
         int jd2 = n_k(t, sr - alpha_min);

         for (int alpha = alpha_min; alpha <= alpha_max; alpha++) {
            // optimizations:
            const int alpha2 = 2 * alpha;
            const int sralpha = sr - alpha;

            int pb = alpha2 - srt;
            int qb = s2rt - alpha2;
            int sb = u;
//          T temp_alpha = Iv1<T>(pb, qb, sb);
            T temp_alpha = Iv2<T>(pb, qb, sb);
            temp_alpha *= bino<T>(jd1++) * bino<T>(jd2--);

            res += temp_alpha;
         }

         temp *= res;
      }

      // S_1( MLam = Lam, Lam, NLam, Kbar):
      {
         int pb = lmgamma;
         int qb = gamma;
         int sb = Kbarplm;
//       T res = Iv1<T>(pb, qb, sb);
         T res = Iv2<T>(pb, qb, sb);
         temp *= res;
      }

      // S_2(kappa_, k, kappa = k, MBar, L, KBar):
      {
         T res = 0.0;

         // int alpha_min = std::max(0, -Kbar - Mbar);
         assert(-Kbar - Mbar <= 0);
         int alpha_min = 0;

         // int alpha_max = std::min(L - Kbar, L - Mbar);
         int alpha_max = std::min(LKbar, LMbar);

         int jd1 = n_k(LKbar, alpha_min);
         int jd2 = n_k(LpKbar, LMbar - alpha_min);

         for (int alpha = alpha_min; alpha <= alpha_max; alpha++) {
            // optimizations:
            const int xyKbarMbaralpha = xyKbarMbar + alpha;

            T temp_alpha = 0.0;

            int beta_min = 0;
            int beta_max = y;

            int id1 = n_k(y, beta_min);
            int id2 = n_k(xL, xyKbarMbaralpha - beta_min);

            for (int beta = beta_min; beta <= beta_max; beta++) {
               assert((x + y + Kbar + Mbar) % 2 == 0);
               // T temp_beta = bino(y, beta) / bino(x + L, (x + y + Kbar + Mbar) / 2 + alpha -
               // beta); T temp_beta = bino(y, beta) * binv(x + L, (x + y + Kbar + Mbar) / 2 + alpha
               // - beta); T temp_beta = bino(y, beta) * binv(xL, xyKbarMbaralpha - beta);
               T temp_beta = bino<T>(id1++) * binv<T>(id2--);

               if (beta % 2) temp_beta = -temp_beta;
               temp_alpha += temp_beta;
            }

            // temp_alpha *= bino(L - Kbar, alpha) * bino(L + Kbar, L - Mbar - alpha);
            // temp_alpha *= bino(LKbar, alpha) * bino(LpKbar, LMbar - alpha);
            temp_alpha *= bino<T>(jd1++) * bino<T>(jd2--);

            if (alpha % 2) temp_alpha = -temp_alpha;
            res += temp_alpha;
         }

         // res /= (x + L + 1);
         res *= xL1inv;
         temp *= res;
      }

      res += temp;
   }

   res *= C;

   return static_cast<double>(res);
}

} // nanespace su3

#endif
