#ifndef SU3_INIT_H
#define SU3_INIT_H

namespace su3
{
   // max_lm_p_mu = max(lm + mu) = max(L)
   // implicitly set max(lm + mu) = 100 
   // if jjmax == -1 ==> set jjmax = 2*max(lm + mu)
   void init(int max_lm_p_mu = 100, int jjmax = -1);
   // max_lm_p_mu = max(lm + mu) = max(L)
   // implicitly set max(lm + mu) = 100
   // if jjmax == -1 ==> set jjmax = 2*max(lm + mu)
   void init_thread(int max_lm_p_mu = 100, int jjmax = -1);

   void finalize();
   void finalize_thread();

} // namespace su3

#endif
