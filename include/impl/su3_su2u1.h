#ifndef SU3_SU3_SU2U1_H
#define SU3_SU3_SU2U1_H

#include "utils.h"

#include <iostream>
#include <map>
#include <vector>

namespace su3 {
struct Generator_LM1LM2eps2toHW {
   int lm1_, lm3_, ia1_, ib1_, ic1_, ie3_;

   Generator_LM1LM2eps2toHW(int lm1, int mu1, int lm3, int mu3);
   int generate(int lm2, int mu2, int nnc, std::vector<int>& j1ta, std::vector<int>& j2ta,
                std::vector<int>& ie2a);
};

int multhy(int lm1, int mu1, int lm2, int mu2, int lm3, int mu3);

// Compute <(lm1 mu1) e1 LM1; (lm2 mu2) e2 LM2 || (lm3 mu3) HW>rho
// Interface identical to xwu3 described in Ayiama & Draayer CPC.
void xewu3(int lm1x, int mu1x, int lm2x, int mu2x, int lm3x, int mu3x, int I3, int& nec, int rhomax,
           std::vector<int>& j1ta, std::vector<int>& j2ta, std::vector<int>& iea2,
           std::vector<double>& xeww);
// Simplified interface to xwu3
// Extreme SU(3)>SU(2)xU(1) coefficients are computed each time xwu3 is executed.
// For large-scale computations may be advisable to store extreme coefficients in fast look-up
// table and call directly xwu3_with_xewu3.
void xwu3(int lm1, int mu1, int lm2, int mu2, int lm3, int mu3, int eps3, int LM3, int rhomax,
          std::vector<std::tuple<int, int, int>>& labels_e2LM2LM1, std::vector<double>& dwu3);
/////////////////////////////////////////////////////////////////////
// Implement relation (20)
//
// true: function reached "GO TO 120" statement
// This happens if in the relation (20) one encounters either
// (1) f(j) == 0 for some j
// or
// (2) the first factor in the relation for G(q2) is equal to 0
//
// NOTE: due to the templatization one can easily avoid overflow problem by choosing appropriate
// type. Therefore, we can eliminate overflow problem handling implemented by Draayer su3lib.
template <typename T>
bool Relation20(int a, int b, int c, int d, int lm1, int mu1, int lm2, int mu2, int nec,
                     int rho, int rhomax, const std::vector<int>& j1ta, std::vector<T>& dewu3p) {
   const int is2 = lm2 + mu2 + 2;
   const int iss = mu1 + 1 + lm2 + mu2 - nec;
   const int nnc = nec + 1;
   // F77: I=0
   // number of e2 LM2 state such that
   // HW1 x (e LM)2 --> HW2
   int num_HW2HW_states = 0;
   // ind should point to < HW; (e2 max(j1ta)); HW>
   // J1TD: 0   ==> e1_HW
   // J1T : lm1 ==> HW
   // J2TD: nec ==> e2=e2_HW +3nec and e1_HW + e2 = e3_HW
   // J2T: lm2 + nec ==> highest possible value
   size_t ind = INDEX(0, lm1, lm1, nec, lm2, (lm2 + nec));
   size_t ki = rho - 1;
   // ki for F77 compatible mapping starting with ki = 1
   //   size_t ki = rho;
   int iiq2b;
   // iterate over (~q2+1) values
   // i.e. iiq2 == (~q2 + 1)
   for (int iiq2 = 1; iiq2 <= nnc; ++iiq2, ki += rhomax, ++ind) {
      // ki --> < (lm1 mu1) HW; (lm2 mu2) e2 LM2; (lm3 mu3) HW>_rho
      dewu3p[ki] = 0.0;
      // given LM2 does not couple LM1HW x LM2 --> LM3HW
      //      if (std::get<0>(j1ta_j2ta_iea2[ind]) < 0)
      if (j1ta[ind] < 0) {
         continue;
      }
      num_HW2HW_states++;
      // j2 + 1
      iiq2b = iiq2;
   }
   // j1 + 1
   int iiq2a = iiq2b - num_HW2HW_states + 1;
   assert(iiq2a <= iiq2b);
   // j2
   int iq2b = iiq2b - 1;
   //   size_t indq = (iiq2a - 1)*rhomax;
   ki = (iiq2a - 1) * rhomax + rho - 1;
   // ki for F77 compatible mapping starting with ki = 1
   //   ki = (iiq2a - 1)*rhomax + rho;
   size_t iz = 0;
   // iterate ove e2 LM2 <--> ~q2 states ...
   // (j1 + 1) <= iiq2 <= (j2 + 1)
   // ==> iiq2 == (~q + 1)
   for (int iiq2 = iiq2a; iiq2 <= iiq2b; ++iiq2, ++iz, ki += rhomax) {
      // L = (lm2 - ~q2)
      int L = (lm2 + 1) - iiq2;
      int ix = L - iiq2 + nnc + 1;
      if (ix == 0) {
         return true;
      }
      ////////////////////////////////////////////
      // Compute G(~q2) as  G(~q2)=exp(dn)*IN
      // where in == -1 for G(~q2) < 0
      //       in == +1 for G(~q2) > 0
      ////////////////////////////////////////////
      int iy = std::abs(ix);
      // ix == 0 case gets eliminated by the previous condition
      // in denotes whether the number represented as log (|G|) is positive or negative.
      int in = ix / iy;
      // dn can get seriously large with increasing rhomax => use log(G) instead
      double dn = std::log(iy);
      // j1 < j2 ==> compute g(j1)*g(j1+1)* ... *g(j2-1)
      if (iiq2a < iiq2b) {
         // i: j1 + 1, ..., j2
         // ==> i = (j + 1)
         // compute of g(j) numbers
         for (int i = iiq2a; i <= iq2b; ++i) {
            // j == (n + 1) - (j + 1) = n - j
            int j = nnc - i;
            // rhomax:21 => K ~ 10^6 ==> int64_t range more than enough
            int64_t K;
            // (j+1) < ~q2+1 => j < ~q2
            if (i < iiq2) {
               // K = g(j) = (a+n-j)(b-n+j)(c+n-j)(d+n-j)((lm2+mu2+2)-(j+1))
               K = (int64_t)(a + j) * (b - j) * (c + j) * (d + j) * (is2 - i);
            } else  // (j1+1) >= (~q2 +1)
            {
               // K = g(j) = (mu2 - n + j + 1)
               K = (mu2 + 1) - j;
            }
            if (K == 0)  // => G(~q2)=0
            {
               return true;
            }
            if (K < 0) {
               in = -in;
            }
            dn += std::log(std::abs(K));
         }
      }
      // long double can store binomial coefficient (nec, k) without loss up to nec = 68.
      // Note: max(nec) = rhomax.
      dn += std::log(bino<long double>(nec, iiq2 - 1));

      ////////////////////////////////////////
      // Compute H(~q2)
      ////////////////////////////////////////
      double dd = 0.0;
      for (int i = 1; i <= nnc; ++i) {
         assert((i + L) > 0);
         dd += std::log(i + L);
      }
      ////////////////////////////////////////
      // Compute F(~p2)
      ////////////////////////////////////////
      // (~p2 ~q2) has form (n-~q2 ~q2)
      // ip2 = ~p2 = n - ~q2
      int ip2 = nnc - iiq2;
      // Computing F(p2) ... it can get large => use higher precision
      // ~p2 + 1
      int iip2 = ip2 + 1;
      T ds = 0;
      for (int i = 1; i <= iip2; ++i) {
         // Can be replaced with quad/oct precision when particularly high rhomax are needed.
         T dc = bino<long double>(ip2, (i - 1));
         // if ~p2 == 0 ==> F(~p2)=1;
         if (iip2 != 1) {
            for (int j = 1; j <= ip2; ++j) {
               double K;
               if (j < i) {
                  K = (j + L) * (iss + j);
               } else {
                  K = (a + j) * (b - j);
               }
               dc = K * dc;
            }
         }
         ds += dc;
      }  // 110
      if (ip2 % 2) {
         ds = -ds;
      }
      assert(in == 1);
      dewu3p[ki] = in * ds * exp((dn - dd) / 2.0);
   }
   return false;
}

// Relation (17)
// C     GENERATE <(LAM1,MU1)????;(LAM2,MU2)HIGH::KR0(LAM3,MU3)HIGH>
// C     FROM <(LAM1,MU1)????;(LAM2-1,MU2-1)HIGH::KR0(LAM3,MU3)HIGH>
template <typename T>
void Relation17(int a, int b, int c, int d, int nec, int lm1, int mu1, int kr0cnt, int rhomax,
                const std::vector<int>& j1ta, const std::vector<int>& j1tap,
                const std::vector<T>& dewu3p, std::vector<T>& su3cgs) {
   int ll1 = lm1 + 1;
   int mm1 = mu1 + 1;
   int is1 = ll1 + mm1;
   int ln1 = lm1 + nec;
   int nnc = nec + 1;
   size_t indq = 0;
   for (size_t ind = 0; ind < nnc; ++ind, indq += rhomax) {
      int j1t = j1ta[ind];

      // IF(J1T.LT.0)GO TO 75
      if (j1t < 0) {
         continue;
      }
      int iq1 = (ln1 - j1t) / 2;
      if (iq1 > 0) {
         int j1tp = j1t + 1;
         // INDP=(LN1-J1TP-1)/2+1
         int indp = (ln1 - j1tp - 1) / 2;
         if (j1tap[indp] >= 0) {
            int64_t i = (int64_t)(a + iq1) * (b - iq1) * iq1 * (ll1 - iq1) * (is1 - iq1);
            double dc = -std::sqrt((double)i / (double)((j1t + 2) * j1tp));
            size_t indpq = indp * rhomax;
            for (size_t kr0 = 0; kr0 < kr0cnt; ++kr0) {
               size_t ki = kr0 + indq;
               size_t kip = kr0 + indpq;
               //                     std::cout << "KI:" << ki+1 << " KIP:" << kip+1 << " DC:"
               //                     << dc << " dewu3p:" << dewu3p[kip] << std::endl;
               su3cgs[ki] = dc * dewu3p[kip];
            }  // 60
         }
      }
      // 65
      int ip1 = nec - iq1;
      if (ip1 > 0) {
         int j1tp = j1t - 1;
         size_t indp = (ln1 - j1tp - 1) / 2;
         if (j1tap[indp] >= 0) {
            int64_t i =
                (int64_t)(c + (nnc - ind - 1)) * (d + (nnc - ind - 1)) * ip1 * (mm1 - ip1) * (ll1 + ip1);
            double dc = std::sqrt((double)i / (double)((j1tp + 2) * j1t));
            size_t indpq = indp * rhomax;
            for (size_t kr0 = 0; kr0 < kr0cnt; ++kr0) {
               size_t ki = kr0 + indq;
               size_t kip = kr0 + indpq;
               //                   std::cout << "KI:" << ki+1 << " KIP:" << kip+1 << " DC:"
               //                   << dc << " dewu3p:" << dewu3p[kip] << std::endl;
               su3cgs[kip] += dc * dewu3p[kip];
            }
         }
      }
   }  // 75
}

template <typename T>
void xewu3s(int inc, int a, int b, int c, int d, int lm1, int mu1, int lm2, int mu2, int nec,
            int kr0a, int kr0b, const std::vector<T>& dewu3p, const std::vector<int>& j1ta,
            int indmax, std::vector<T>& su3cgs, int rhomax) {
   int nnc = nec + 1;

   size_t indq = 0;
   if (inc == 1) {
      indq = indq + (indmax - nnc) * rhomax;
   }

   size_t indpq = 0;
   for (int iiq = 1; iiq <= nnc; ++iiq, indq += rhomax, indpq += rhomax) {
      for (int kr0 = kr0a; kr0 <= kr0b; ++kr0) {
         size_t ki = kr0 + indq - 1;
         size_t kip = kr0 + indpq - 1;
         su3cgs[ki] = dewu3p[kip];
      }
   }
   if (nec == 0) {
      return;
   }
   int l1, m1, l2, m2;
   if (inc == 0) {
      l1 = lm1;
      m1 = mu1;
      l2 = lm2;
      m2 = mu2;
   } else  // 15
   {
      l1 = lm2;
      m1 = mu2;
      l2 = lm1;
      m2 = mu1;
   }
   int ll1 = l1 + 1;
   int mm1 = m1 + 1;
   int ll2 = l2 + 1;
   int mm2 = m2 + 1;
   int LM1 = ll1 + mm1;
   int LM2 = ll2 + mm2;
   for (int j2td = 1; j2td <= nnc; ++j2td) {
      int j1td = nec - j2td;
      int j2d = j2td - 1;
      int j1d = j1td + 1;
      int iiq2a = j2td - m2;
      if (iiq2a < 0) {
         iiq2a = 0;
      }
      iiq2a = iiq2a + 1;
      int iiq2b = j2td;
      if (l2 < iiq2b) {
         iiq2b = l2;
      }
      iiq2b = iiq2b + 1;
      int iiq1a = j1td - m1;
      if (iiq1a < 0) {
         iiq1a = 0;
      }
      iiq1a = iiq1a + 1;
      int iiq1b = j1td;
      if (l1 < iiq1b) {
         iiq1b = l1;
      }
      iiq1b = iiq1b + 1;
      for (int iiq2 = iiq2a; iiq2 <= iiq2b; ++iiq2) {
         int iq2 = iiq2 - 1;
         int ip2 = j2td - iq2;
         int j2t = l2 + ip2 - iq2;
         int jj2t = j2t + 1;
         int iq = -1;
         int ip = -1;

         int iq2p, ip2p, iq2d, ip2d;
         int dm, dn;
         if (ip2 != 0) {
            iq2p = iq2;
            ip2p = ip2 - 1;
            iq2d = 0;
            ip2d = 1;
            if (inc == 1) {
               ip = 1;
            }
            dm = ip2 * (m2 - ip2p) * (ll2 + ip2);
            dn = j2t;
         } else  // 25
         {
            iq2p = iq2 - 1;
            ip2p = ip2;
            iq2d = 1;
            ip2d = 0;
            if (inc == 0) {
               iq = 1;
            }
            dm = iq2 * (l2 - iq2p) * (LM2 - iq2);
            dn = j2t + 2;
         }
         int j2tp = l2 + ip2p - iq2p;
         int jta = j2td - iq2p;
         int jtb = nnc - jta;
         int nqd = nec - iq2p;
         for (int iiq1 = iiq1a; iiq1 <= iiq1b; ++iiq1) {
            int iq1 = iiq1 - 1;
            int ip1 = j1td - iq1;
            int j1t = l1 + ip1 - iq1;
            size_t ind;
            if (inc == 0) {
               ind = INDEX(j1td, l1, j1t, j2td, l2, j2t);
            }
            if (inc == 1) {
               ind = INDEX(j2td, l2, j2t, j1td, l1, j1t);
            }
            if (j1ta[ind] < 0) {
               continue;
            }

            size_t indp;
            int j123;
            int iq1p, ip1p, j1tp;
            if (ip1 != m1)  // if ip1 == m1 ==> GO TO 50
            {
               iq1p = iq1;
               ip1p = ip1 + 1;
               j1tp = j1t + 1;
               if (inc == 1) {
                  indp = INDEX(j2d, l2, j2tp, j1d, l1, j1tp);
                  if (j1ta[indp] >= 0) {
                     j123 = jtb - iq1;
                  }
               } else {
                  indp = INDEX(j1d, l1, j1tp, j2d, l2, j2tp);
                  if (j1ta[indp] >= 0) {
                     j123 = jta + iq1;
                  }
               }
               if (j1ta[indp] >= 0) {
                  int i;
                  if (ip2d == 1) {
                     i = (a + j123) * (b - j123);
                  }
                  if (iq2d == 1) {
                     i = (c + nqd - iq1) * (d + nqd - iq1);
                  }
                  int64_t dx = (int64_t)jj2t * ip1p * (mm1 - ip1p) * ((ll1 + ip1p) * i);
                  using std::sqrt;
                  using boost::multiprecision::sqrt;
                  // higher precision required for large rhomax values
                  T dc = sqrt((T)dx / (T)((int64_t)(j1t + 2) * j1tp * dm * dn));
                  if (iq < 0) {
                     dc = -dc;
                  }
                  size_t indq = ind * rhomax;
                  size_t indpq = indp * rhomax;
                  for (int kr0 = kr0a; kr0 <= kr0b; ++kr0) {
                     size_t ki = kr0 + indq - 1;
                     size_t kip = kr0 + indpq - 1;
                     su3cgs[ki] = dc * su3cgs[kip];
                  }
               }
            }
            // 50
            if (iq1 == l1) {
               continue;
            }
            int i;
            iq1p = iq1 + 1;
            ip1p = ip1;
            j1tp = j1t - 1;
            if (inc == 1) {
               indp = INDEX(j2d, l2, j2tp, j1d, l1, j1tp);
               if (j1ta[ind] < 0) {
                  continue;
               }
               j123 = jtb - iq1;
            } else {
               indp = INDEX(j1d, l1, j1tp, j2d, l2, j2tp);
               if (j1ta[ind] < 0) {
                  continue;
               }
               j123 = jta + iq1;
            }
            if (ip2d == 1) {
               i = (c + nqd - iq1) * (d + nqd - iq1);
            }
            if (iq2d == 1) {
               i = (a + j123) * (b - j123);
            }
            T dx = (int64_t)(jj2t) * (iq1p) * (ll1 - iq1p) * ((LM1 - iq1p) * i);
            using std::sqrt;
            using boost::multiprecision::sqrt;
            // higher precision required for large rhomax values
            T dc = sqrt((T)dx / (T)((int64_t)(j1tp + 2) * j1t * dm * dn));
            if (ip < 0) {
               dc = -dc;
            }
            size_t indq = (ind)*rhomax;
            size_t indpq = (indp)*rhomax;
            for (int kr0 = kr0a; kr0 <= kr0b; ++kr0) {
               size_t ki = kr0 + indq - 1;
               size_t kip = kr0 + indpq - 1;
               su3cgs[ki] += dc * su3cgs[kip];
            }
         }  // 70
      }     // 70
   }        // 70
}

// Compute extreme CGs using in precision type T.
// Return result as vector<T>.
//
// This is needed for accurate computations of SU(3)>SU(2)xU(1) CGs with rhomax >= 12
template <typename T>
void xewu3_internal(int lm1x, int mu1x, int lm2x, int mu2x, int lm3x, int mu3x, int I3, int& nec,
                    int rhomax, std::vector<int>& j1ta, std::vector<int>& j2ta,
                    std::vector<int>& iea2, std::vector<T>& su3cgs) {
   std::vector<T> dewu3p;

   int inc;
   int indmax;

   assert(rhomax == su3::mult(lm1x, mu1x, lm2x, mu2x, lm3x, mu3x));

   int lm1, mu1, lm2, mu2, lm3, mu3;
   if (I3 != 1) {
      lm1 = mu1x;
      lm2 = mu2x;
      lm3 = mu3x;
      mu1 = lm1x;
      mu2 = lm2x;
      mu3 = lm3x;
   } else {
      lm1 = lm1x;
      lm2 = lm2x;
      lm3 = lm3x;
      mu1 = mu1x;
      mu2 = mu2x;
      mu3 = mu3x;
   }

   Generator_LM1LM2eps2toHW LM1LM2eps2toHW(lm1, mu1, lm3, mu3);

   int nnc;
   // see definitions of n, a, b, c, d in relation (20) Draayer & Akiyama
   nec = (lm1 + lm2 - lm3 + 2 * (mu1 + mu2 - mu3)) / 3;
   int a = (lm2 + lm3 - lm1 - nec) / 2;
   int b = (lm3 + lm1 - lm2 + nec + 2) / 2;
   int c = (lm1 + lm2 - lm3 - nec) / 2;
   int d = (lm1 + lm2 + lm3 - nec + 2) / 2;

   std::vector<int> j1tap((nec + 1), 0.0);
   // maximal size of dewu3p array
   dewu3p.resize((nec + 1) * rhomax, 0.0);

   // \eta_{\max} defined on page 1906 Draayer & Akiyama
   int ncdmax = multhy(lm1, mu1, lm2, mu2, lm3, mu3);
   nec = nec - ncdmax;
   lm2 = lm2 - ncdmax;  // (lm1 mu1) x (lm2 mu2) --> (lm3 mu3) not allowed!
   mu2 = mu2 - ncdmax;

   // from the definition ncdmax this should hold!
   assert(su3::mult(lm1, mu1, lm2 + 1, mu2 + 1, lm3, mu3) == 1);

   int ncdmin = 1;
   if (ncdmin != ncdmax) {
      while (su3::mult(lm1, mu1, lm2 + 1, mu2 + 1, lm3, mu3) <= 0) {
         // Is this branch ever executed given the fact that in condition
         // is guaranteed to be su3mult == 1?
         nec = nec + 1;
         lm2 = lm2 + 1;
         mu2 = mu2 + 1;
         ncdmin = ncdmin + 1;
      }
   }

   int nncmax = nec + ncdmax - ncdmin + 2;
   // number of su3cgs
   size_t kitest = rhomax * nncmax * (nncmax + 1) * (nncmax + 2) / 6;
   assert(su3cgs.empty());
   su3cgs.resize(kitest, 0.0);

   // epsilon3 <-- epsilon_HW
   int ie3 = -(lm3 + 2 * mu3);

   int kr0cnt = 0;
   // iterate over multiplicity rho == ncd
   for (int ncd = ncdmin; ncd <= ncdmax; ++ncd) {
      nec = nec + 1;
      lm2 = lm2 + 1;
      mu2 = mu2 + 1;
      nnc = nec + 1;
      indmax = nnc * (nnc + 1) * (nnc + 2) / 6;

      int inn = nec * nnc / 2;
      if (ncd != ncdmin) {
         std::fill(su3cgs.begin(), su3cgs.begin() + kitest, 0.0);
      }

      j1ta.resize(indmax);
      j2ta.resize(indmax);
      iea2.resize(indmax);
      std::fill(j1ta.begin(), j1ta.end(), -1000);
      std::fill(j2ta.begin(), j2ta.end(), -1000);
      std::fill(iea2.begin(), iea2.end(), -1000);

      int I = LM1LM2eps2toHW.generate(lm2, mu2, nnc, j1ta, j2ta, iea2);

      if (I == 0) {
         continue;
      }

      if (kr0cnt != 0) {
         Relation17(a, b, c, d, nec, lm1, mu1, kr0cnt, rhomax, j1ta, j1tap, dewu3p, su3cgs);
         inc = 0;
      }

      if (kr0cnt < rhomax) {
         kr0cnt = kr0cnt + 1;

         // use relation (20) to <1HW; 2||3HW>_rho
         bool goto120 =
             Relation20(a, b, c, d, lm1, mu1, lm2, mu2, nec, kr0cnt, rhomax, j1ta, dewu3p);

         if (!goto120) {
            //     GENERATE <(LAM1,MU1)????;(LAM2,MU2)????::KR0(LAM3,MU3)HIGH>
            //     FROM <(LAM1,MU1)HIGH;(LAM2,MU2)????::KR0(LAM3,MU3)HIGH>
            int kr0a = kr0cnt;
            int kr0b = kr0cnt;
            xewu3s(1, a, b, c, d, lm1, mu1, lm2, mu2, nec, kr0a, kr0b, dewu3p, j1ta, indmax, su3cgs,
                   rhomax);
            inc = 1;
         } else
         // this branch is true if in relation (20) we get f(j)/g(j) that are equal to zero
         {
            kr0cnt = kr0cnt - 1;
            // Error message from XEWU3.F
            std::cerr << "***** U3 COUPLING ERROR *****" << std::endl;
            std::cerr << "***** REPORT TO AUTHOR *****" << std::endl;
            exit(EXIT_FAILURE);
         }
      }

      if (kr0cnt == 0) {
         continue;
      }
      j1tap.resize(nnc, 0.0);
      size_t indq = 0;
      // iterate over ~q2 values?
      for (size_t ind = 0; ind < nnc; ++ind, indq += rhomax) {
         j1tap[ind] = j1ta[ind];
         for (size_t irho = 0; irho < kr0cnt; ++irho) {
            size_t ki = irho + indq;
            dewu3p[ki] = su3cgs[ki];
         }
      }
   } 

   if (kr0cnt == 0) {  // this should never happen
      return;
   }

   int kr0a = 1;
   int kr0b = kr0cnt - inc;
   if (kr0b != 0) {
      //    GENERATE <(LAM1,MU1)????;(LAM2,MU2)????::KR0(LAM3,MU3)HIGH>
      //    FROM <(LAM1,MU1)????;(LAM2,MU2)HIGH::KR0(LAM3,MU3)HIGH>
      xewu3s(0, a, b, c, d, lm1, mu1, lm2, mu2, nec, kr0a, kr0b, dewu3p, j1ta, indmax, su3cgs,
             rhomax);
   }

   // su3cgs can be huge (~10^91 for rhomax=24)
   // ==> for each rho, divide by the largest rho CG
   for (int kr0 = 0; kr0 < kr0cnt; ++kr0) {
      T dc = 1.0;
      size_t ki = kr0;
      for (int ind = 1; ind <= indmax; ++ind, ki += rhomax) {
         using boost::multiprecision::fabs;
         using std::fabs;
         dc = std::max(dc, fabs(su3cgs[ki]));
      }

      ki = kr0;
      for (int ind = 1; ind <= indmax; ++ind, ki += rhomax) {
         su3cgs[ki] = su3cgs[ki] / dc;
      }
   }
   ///////

   for (int kr0 = 1; kr0 <= kr0cnt; ++kr0) {
      int kr0pa = 1;
      if ((inc == 1) && (kr0 == kr0cnt)) {
         kr0pa = kr0cnt;
      }

      for (int kr0p = kr0pa; kr0p <= kr0; ++kr0p) {
         T dn = 0;
         size_t ki = kr0 - 1;
         size_t kip = kr0p - 1;
         for (int ind = 1; ind <= indmax; ++ind, ki += rhomax, kip += rhomax) {
            dn += su3cgs[ki] * su3cgs[kip];
         } 
         if (kr0p < kr0) {
/*            
            // this condition reduces computing time but impacts accuracy 
            // particularly for higher precision datatypes
            using boost::multiprecision::abs;
            using std::abs;
            // results
            if (abs(dn) < 1.0e-12) {
               continue;
            }
*/            
            ki = kr0 - 1;
            kip = kr0p - 1;
            for (int ind = 1; ind <= indmax; ++ind, ki += rhomax, kip += rhomax) {
               su3cgs[ki] -= dn * su3cgs[kip];
            }  
         } 
         else 
         {
            using boost::multiprecision::sqrt;
            using std::sqrt;
            dn = 1.0 / sqrt(dn);
            ki = kr0 - 1;
            for (int ind = 1; ind <= indmax; ++ind, ki += rhomax) {
               su3cgs[ki] = dn * su3cgs[ki];
            }
         }
      }
   }  

   // SET PHASE CONVENTION (K.T.HECHT, NUCL.PHYS.62(1965)1)
   // According to paper: <(lm1 mu1) LW; (lm2 mu2) e2 LM2max || (lm3 mu3) LW>_rho > 0
   int iph = 2 * (lm1 + lm2 - lm3 + mu1 + mu2 - mu3 + rhomax);
   // Step 1:
   // find <(lm1 mu1) HW; (lm2 mu2) e2 LM2_max || (lm3 mu3) HW>_rho=0
   // where e2 = e3_HW - e1_HW
   size_t ind = std::find(iea2.begin(), iea2.end(), ie3 + (lm1 + 2 * mu1)) - iea2.begin();
   // i = 2*phi + 2*rhomax + 2*(LM_1 + 2LM_2 - LM_HW)
   int i = iph + j1ta[ind] + j2ta[ind] - lm3;
   size_t ki = ind * rhomax;
   // iterate over <(lm1 mu1) HW; (lm2 mu2) e2 LM2max|| (lm3 mu3) HW>_rho=*
   for (int kr0 = 0; kr0 < rhomax; ++kr0, ++ki) {
      // i = 2*phi + 2*(rhomax - rho) + 2*(LM_1 + 2LM_2 - LM_HW)
      i = i - 2;
      int j = i;
      if (su3cgs[ki] < 0) {
         j = j - 2;
      }
      if (j % 4) {  // if (true) ==> <(lm1 mu1) LW; (lm2 mu2) e2 LM2max || (lm3 mu3) LW>_kr0 < 0
         // multiply all <(lm1 mu1) ???; (lm2 mu2) ??? || (lm3 mu3) HW>_rho=kr0 by -1
         size_t kip = kr0;
         for (int indp = 0; indp < indmax; ++indp, kip += rhomax) {
            su3cgs[kip] = -su3cgs[kip];
         }
      }
   }

   // Apply relation 2B. to compute
   // <G1 G2 | G_3LW>_rho = (-)^phi <~G1 ~G2||~G3_HW>
   if (I3 == 0) {  // G_3E = G_3LW/LW'
      // su3cgs= {<~G1 ~G2 || ~G3_HW>}[ind][rho]
      size_t ki = 0;
      for (int ind = 0; ind < indmax; ++ind) {
         if (j1ta[ind] < 0) {
            ki += rhomax;
            continue;
         }
         //  note lm3 == mu3x if I3 == 0
         //  i = 2*phi + 2*rhomax + 2*(LM_1 + 2LM_2 - LM_3LW), where
         int i = iph + j1ta[ind] + j2ta[ind] - lm3;
         for (int kr0 = 0; kr0 < rhomax; ++kr0, ++ki) {
            // i <-- 2*(phi + rhomax - rho + LM_1 + 2LM_2 - LM_3LW)=2*phase
            i = i - 2;
            // i%4 = (2*phase)%4 = phi%2
            if (i % 4) {
               su3cgs[ki] = -su3cgs[ki];
            }
         }
      }
      // Note: some iea2 elements are equal to -1000 => become 1000
      // conjugate e2 --> -e2
      std::transform(iea2.begin(), iea2.end(), iea2.begin(),
//                   std::bind1st(std::multiplies<int>(), -1));
                     std::bind(std::multiplies<int>(), -1, std::placeholders::_1));
   }
}

// Compute extreme CGs using in precision type T.
// Return result as vector<double>.
template <typename T>
void xewu3_internal_to_dbl(int lm1x, int mu1x, int lm2x, int mu2x, int lm3x, int mu3x, int I3, int& nec,
                    int rhomax, std::vector<int>& j1ta, std::vector<int>& j2ta,
                    std::vector<int>& iea2, std::vector<double>& su3cgs_results) {
   std::vector<T> su3cgsT;
   xewu3_internal(lm1x, mu1x, lm2x, mu2x, lm3x, mu3x, I3, nec, rhomax, j1ta, j2ta, iea2, su3cgsT);
   su3cgs_results.resize(su3cgsT.size());
   for (size_t i = 0; i < su3cgsT.size(); ++i) {
      su3cgs_results[i] = (double)su3cgsT[i];
   }
}

// Comoute SU3>SU2xU1 CGs using provided set of extreme CGs <(lm1 mu1) e1 LM1; (lm2 mu2) e2 LM2 ||
// (lm3 mu3) HW>
//
// Compute SU(3) > SU(2) x U(1) Wigner coefficients.
// Interface identical to xwu3 described in Ayiama & Draayer CPC.
template <typename T>
void xwu3_internal(int lm1, int mu1, int lm2, int mu2, int lm3, int mu3, int eps3, int LM3,
                     const std::vector<T>& dewu3, int rhomax, int& indmax,
                     std::vector<int>& j1smax, std::vector<int>& j1tmax, std::vector<int>& j2smax,
                     std::vector<int>& j2tmax, int& ie2max, std::vector<int>& indmat,
                     std::vector<T>& dwu3) {
   // nec:
   // In F77 nec is a parameter of xwu3 and it is being computed by xewu3.
   // xewu3 is called with I3=1 ==> HW/HW'
   // and xewu3 therefore returns nec = (lm1 + lm2 - lm3 + 2 * (mu1 + mu2 - mu3)) / 3;
   // (dewu3 therefore contains <(lm1 mu1) (lm2 mu2) || (lm3 mu3) HW>})
   int nec = (lm1 + lm2 - lm3 + 2 * (mu1 + mu2 - mu3)) / 3;

   int iesmax;
   // estimating maximal possible number of CG coefficients
   // question: is this an estimate or known fact?
   int idtest = rhomax * su3::dim(lm2, mu2);
   dwu3.assign(idtest, 0.0);
   std::vector<T> dwu3p(idtest, 0.0);

   // n2: maximal number of different epsilon_2 numbers
   // and also maximal number of different Lambda_2 numbers
   // MAX(IESMAX) and MAX(J2SMAX)
   // F77 compatible const int n2 = NCW2;
   // We use dynamically computed value
   const int n2 = lm2 + mu2 + 1;

   // Set sizes of output arrays
   j1tmax.assign(n2 * n2, 0);
   j1smax.assign(n2 * n2, 0);
   j2tmax.assign(n2, 0);
   j2smax.assign(n2, 0);
   indmat.assign(n2 * n2, 0);

   // J2TMAP(X2), where X2=N2=max(iesmax)=max(lm2 + mu2 + 1)
   std::vector<int> j2tmap(n2, 0);
   // INDMAP(X3), where X3=N2*NB=MAX(IESMAX)*MAX(J2SMAX)=max(lm2+mu2+1)*max(lm2 + mu2 + 1)
   std::vector<int> indmap(n2 * n2, 0);

   int nm;
   int jj2tda, jj2tdb, jj2tdc;
   int j3tp;
   // Notice that jj2tdp is not initialized ...
   int jj2tdp;
   int iab, icd, iph;

   int ll1 = lm1 + 1;
   int mm1 = mu1 + 1;
   int ll2 = lm2 + 1;
   int mm2 = mu2 + 1;
   int ll3 = lm3 + 1;
   int mm3 = mu3 + 1;
   int LM1 = lm1 + mu1;
   int LM2 = lm2 + mu2;
   int llmm1 = ll1 + mm1;
   int llmm2 = ll2 + mm2;
   int llmm3 = ll3 + mm3;
   // number of steps from esp3_HW + 1
   int jjtd = (eps3 + lm3 + 2 * mu3) / 3 + 1;
   int ip = (jjtd + LM3 - ll3) / 2;
   int ncc = nec - 1;
   int inc = 1;
   // q3
   int iq3 = 0;
   // p3
   int ip3 = -1;
   // j3t = 2LM_{3HW} - 1
   int j3t = lm3 + ip3 - iq3;

   int ind, ies;
   // iterate from eps3_HW ... eps3
   for (int jj3td = 1; jj3td <= jjtd; ++jj3td) {
      int eps3 = -lm3 - 2 * mu3 + 3 * (jj3td - 1);
      int eps3p = eps3 + 3;
      ncc = ncc + 1;
      // important condition: switching to inc=0
      if (ip3 == ip) {
         inc = 0;
      }

      if (inc != 1) {  // inc == 0
         iq3 = iq3 + 1;
         // 2Lambda_3' = 2Lambda_3 - 1
         j3t = j3t - 1;
         // Note that in the next jj3td iteration, l3t will become 2Lambda_3
         // S(q3+1) ... differs from definition of Draayer & Akiyama
         nm = (ll3 - iq3) * iq3 * (llmm3 - iq3);
      } else {
         ip3 = ip3 + 1;
         // 2Lambda_3' = 2Lambda_3 + 1
         j3t = j3t + 1;
         // Note that in the next jj3td iteration, l3t will become 2Lambda_3
         // R(p3+1) ... differs from definition of Draayer & Akiyama
         nm = (mm3 - ip3) * ip3 * (ll3 + ip3);
      }

      // set ranges for iteration over n for which
      // eps1 = eps1_HW + 3*n
      // eps2 = eps2_HW + 3*(ncc - n)
      jj2tda = ncc - LM1;
      if (jj2tda < 0) {
         jj2tda = 0;
      }
      jj2tda = jj2tda + 1;

      jj2tdb = LM2;
      if (ncc < jj2tdb) {
         jj2tdb = ncc;
      }
      jj2tdb = jj2tdb + 1;

      jj2tdc = jj2tda;
      ind = 0;
      int indq = 0;
      // ies ... related to ieps2
      ies = 0;
      // iterate over eps1 eps2 pairs that coupled to eps3
      for (int jj2td = jj2tda; jj2td <= jj2tdb; ++jj2td) {
         int j2ts;
         // n --> eps2 = eps2_Hw + 3*n
         int j2td = jj2td - 1;
         // (ncc - n) --> eps1 = eps1_HW + 3*(ncc - n)
         int j1td = ncc - j2td;
         // ies - 1 = ieps2
         ies = ies + 1;

         // set ranges for iteration over (p2, q2)
         int iiq2a = j2td - mu2;
         if (iiq2a < 0) {
            iiq2a = 0;
         }
         iiq2a = iiq2a + 1;

         int iiq2b = j2td;
         if (lm2 < iiq2b) {
            iiq2b = lm2;
         }
         iiq2b = iiq2b + 1;

         // set ranges for iteration over (p1, q1)
         int iiq1a = j1td - mu1;
         if (iiq1a < 0) {
            iiq1a = 0;
         }
         iiq1a = iiq1a + 1;

         int iiq1b = j1td;
         if (lm1 < iiq1b) {
            iiq1b = lm1;
         }
         iiq1b = iiq1b + 1;

         // iLM_2
         int j2s = 0;
         // iterate over (p2, q2) (that carry eps2?)
         for (int iiq2 = iiq2a; iiq2 <= iiq2b; ++iiq2) {
            int j1ts;
            // IMPORTANT iiq2 --> (p2 q2) !
            int iq2 = iiq2 - 1;
            int ip2 = j2td - iq2;
            // epsilon_2
            int eps2 = -lm2 - 2 * mu2 + 3 * (ip2 + iq2);
            // 2Lambda_{2}
            int j2t = lm2 + ip2 - iq2;
            int j23s = j2t + j3t;
            int j23d = j3t - j2t;
            int j23h = std::abs(j23d);
            int j1s = 0;
            // iterate over (p1 q1) (that carry eps1?) with 2LM1 decreasing
            for (int iiq1 = iiq1a; iiq1 <= iiq1b; ++iiq1) {
               // iiq1 --> (p1 q1)
               int iq1 = iiq1 - 1;
               int ip1 = j1td - iq1;
               // epsilon_1
               int eps1 = -lm1 - 2 * mu1 + 3 * (ip1 + iq1);
               assert(eps1 + eps2 == eps3);
               // 2Lambda_{1}
               int j1t = lm1 + ip1 - iq1;
               // check Lambda_1 x Lambda_2 --> Lambda_3 couplings
               if (j1t < j23h) {
                  continue;
               }
               if (j1t > j23s) {
                  continue;
               }

               // Each iteration decreases value of j1t!
               // ==> j1ts contains 2LM_1^min
               j1ts = j1t;
               j2ts = j2t;

               ind = ind + 1;

               j1s = j1s + 1;
               if (jj3td != 1) {  // not eps2_{HW}
                  // <(lm1 mu1) ep1 j1t; (lm2 mu2) eps2 j2t || (lm3 mu3) eps3 j3t>
                  // Compute <(lm1 mu1) ep1 2Lambda_1; (lm2 mu2) eps2 2Lambda_2 || (lm3 mu3) eps3
                  // 2Lamba_3> using relation (19)
                  int ja = (j23s - j1t) / 2;
                  int jja = ja + 1;
                  int jb = (j23d + j1t) / 2;
                  int jjb = jb + 1;
                  int jc = (j1t + j23s) / 2 + 1;
                  int jjc = jc + 1;
                  int jd = (j1t - j23d) / 2 + 1;
                  int jjd = jd - 1;
                  // jj2tdp == previous jj2td
                  // iesp - 1 == ie2' --> iesp + 1 => eps2' =
                  int iesp = j2td - jj2tdp;
                  // epsilon_2'
                  int eps2p;
                  // epsilon_1'
                  int eps1p;
                  for (int i = 1; i <= 4; ++i) {
                     T dc;
                     int j2tp, j1tp;
                     int m, n;
                     int j12tp;
                     if (i == 1 || i == 2) {
                        // <(lm1 mu1) eps1 j1t; (lmu mu2) eps2-3 j2tp || (lm3 mu3) eps3-3 j3tp>
                        eps1p = eps1;
                        eps2p = eps2 - 3;
                     }
                     if (i == 3 || i == 4) {
                        // <(lm1 mu1) eps1-3 j1tp; (lmu mu2) eps2 j2t || (lm3 mu3) eps3-3 j3tp>
                        eps1p = eps1 - 3;
                        eps2p = eps2;
                     }

                     if (i == 1 || i == 3) {
                        // if i == 1 --> set eps2' to point to eps2 - 3
                        // if i == 3 --> set eps2' to point to eps2
                        iesp = iesp + 1;
                     }
                     // if eps2 is out of allowed range => next iteration
                     if (iesp < 1 || iesp > iesmax) {
                        continue;
                     }

                     // Case1:
                     if (i == 1) {
                        // 2Lambda_2' = 2Lambda_2 + 1
                        j2tp = j2t + 1;
                        // 2Lambda_1' = 2\Lambda_{1}
                        j1tp = j1t;
                        if (j1tp < std::abs(j2tp - j3tp) || j1tp > (j2tp + j3tp)) {
                           continue;
                        }
                        m = iq2;
                        if (m == 0) {
                           continue;
                        }
                        // n = (lm2 + 1 - q2)
                        n = ll2 - m;
                        // n = (lm2 + mu2 + 2 - q2)*(lm2 + 1 - q2)
                        n = (llmm2 - m) * n;
                        j12tp = j2t + 1;
                        if (inc != 1) {
                           iab = jja;
                           icd = jjc;
                           iph = 1;
                        } else {
                           iab = jb;
                           icd = jd;
                           iph = -1;
                        }
                     }

                     if (i == 2) {  // 35
                        // 2Lambda_2' = 2Lambda_2 - 1
                        j2tp = j2t - 1;
                        // 2Lambda_1' = 2Lambda_1
                        j1tp = j1t;
                        if (j1tp < std::abs(j2tp - j3tp) || j1tp > (j2tp + j3tp)) {
                           continue;
                        }
                        m = ip2;
                        if (m == 0) {
                           continue;
                        }
                        n = mm2 - m;
                        n = (llmm2 - n) * n;
                        j12tp = j2t;
                        if (inc != 1) {
                           iab = jjb;
                           icd = jjd;
                           iph = 1;
                        } else {
                           iab = ja;
                           icd = jc;
                           iph = 1;
                        }
                     }

                     if (i == 3) {
                        // 2Lambda_2' = 2Lambda_2
                        j2tp = j2t;
                        // 2Lambda_1' = 2Lambda_1 + 1
                        j1tp = j1t + 1;
                        if (j1tp < std::abs(j2tp - j3tp) || j1tp > (j2tp + j3tp)) {
                           continue;
                        }
                        m = iq1;
                        if (m == 0) {
                           continue;
                        }
                        n = ll1 - m;
                        n = (llmm1 - m) * n;
                        j12tp = j1t + 1;
                        if (inc != 1) {
                           iab = jjb;
                           icd = jjc;
                           iph = 1;
                        } else {
                           iab = ja;
                           icd = jd;
                           iph = 1;
                        }
                     }

                     if (i == 4) {
                        // 2Lambda_2' = 2Lambda_2
                        j2tp = j2t;
                        // 2Lambda_1' = 2Lambda_1 - 1
                        j1tp = j1t - 1;
                        if (j1tp < std::abs(j2tp - j3tp) || j1tp > (j2tp + j3tp)) {
                           continue;
                        }
                        m = ip1;
                        if (m == 0) {
                           continue;
                        }
                        n = mm1 - m;
                        n = (llmm1 - n) * n;
                        j12tp = j1t;
                        if (inc != 1) {
                           iab = jja;
                           icd = jjd;
                           iph = -1;
                        } else {
                           iab = jb;
                           icd = jc;
                           iph = 1;
                        }
                     }
                     // 65
                     if (j12tp <= 0) {
                        if (inc == 1) {
                           iab = 1;
                        }
                        if (inc == 0) {
                           icd = 1;
                        }
                        dc = 1.0;
                     } else {
                        dc = j12tp * (j12tp + 1);
                     }
                     using std::sqrt;
                     using boost::multiprecision::sqrt;
                     dc = sqrt((T)(iab * icd * m * n) / (T)(nm * dc));
                     if (iph < 0) {
                        dc = -dc;
                     }
                     // ie2' = iesp - 1;
                     // find "inpdq" pointing to appropriate CG coefficient from the previous level
                     // <(lm1 mu1) eps1 j1tp (lm2 mu2) eps2 j2tp || (lm3 mu3) (eps3 - 3)
                     // 2Lambda_3'>rho:0
                     // <(lm1 mu1) eps1 2Lambda_1' (lm2 mu2) eps2 2Lambda_2' || (lm3 mu3) (eps3 - 3)
                     // 2Lambda_3'>rho:0 j2tmap[ie2'] = 2Lamda_2'^max int j2sp = (j2tmap[ie2'] -
                     // 2Lambda_2' + 2) / 2; j2sp = iLambda_{2}' + 1 ==> iLambda_2' = j2sp - 1
                     int j2sp = (j2tmap[iesp - 1] - j2tp + 2) / 2;
                     // int indp = (indmap[iLambda_2'][ie2'] - 2Lambda_1') / 2;
                     assert((iesp - 1) < n2 && j2s < n2);
                     int indp = (indmap[iesp - 1 + (j2sp - 1) * n2] - j1tp) / 2;
                     // indpq -->
                     // <(lm1 mu1) eps1 2Lambda_1' (lm2 mu2) eps2 2Lambda_2' || (lm3 mu3) (eps3 - 3)
                     // 2Lambda_3'>rho:0
                     int indpq = (indp - 1) * rhomax;
                     // indq --> <(lm1 mu1) eps1 j1t (lm2 mu2) eps2 j2t || (lm3 mu3) eps3 j3t>
                     int kip = indq;
                     for (int rho = 0; rho < rhomax; ++rho) {
                        dwu3[kip++] += dc * dwu3p[indpq++];
                     }   // 80
                  }      // 85
               } else {  // eps3_{HW}
                  // 90
                  // indpq --> <(lm1 mu1) eps1 2LM_1; (lm2 mu2) eps2 2LM_2 || (lm3 mu3) eps2_HW
                  // 2LM_3HW>_rho:0
                  int indpq = su3::INDEX(j1td, lm1, j1t, j2td, lm2, j2t) * rhomax;
                  // always indq = 0
                  int kip = indq;
                  for (int rho = 0; rho < rhomax; ++rho) {
                     dwu3[kip++] = dewu3[indpq++];
                  }
                  // dwu3[0 ... rhomax-1] --> {<(lm1 mu1) eps1 2LM_1; (lm2 mu2) eps2 2LM_2 || (lm3
                  // mu3) eps2_HW 2LM_3HW>_rho}
               }
               // we are done with (19) or HWS CGs
               // dwu3[indq] = <(lm1 mu1) ep1 LM1; (lm2 mu2) eps2 LM2 || (lm3 mu3) eps LM3>_rho:0
               // we are done with
               // move index pointing the next LM_1
               indq += rhomax;
            }                // iterating over LM1 \in eps1 {(q1 p1)}
            if (j1s == 0) {  // there was no iteration over (q1 p1)
               continue;
            }
            // j2s == iLM_2
            // ies - 1 == ieps2
            // iesj2s --> [iLM_2][ieps2]
            assert((ies - 1) < n2 && j2s < n2);
            int iesj2s = ies + j2s * n2 - 1;
            // iLM_2 = iLM_2 + 1
            j2s = j2s + 1;
            // j1s == number of different LM_1 associated with eps2 LM2
            j1smax[iesj2s] = j1s;
            // 2LM_1^{max} = 2LM_1^min + 2 * (j1s - 1)
            j1tmax[iesj2s] = j1ts + 2 * (j1s - 1);
            // INDMAT(IESJ2S)=2*IND+J1TS
            // j1ts = 2LM_1^min
            indmat[iesj2s] = 2 * ind + j1ts;  // F77 compatible
            // indmat[iesj2s] = 2 * ind + j1ts - 1; //C++ compatible
         }  // iteration over 2LM2
         // j2s ==> number of different \Lambda_{2} in eps_2 is equal to zero
         if (j2s == 0) {
            ies = ies - 1;
            if (ies == 0) {
               jj2tdc = jj2tdc + 1;
            }
         } else {
            // j2smax[ieps2] = number of different Lambda_2 in eps_2
            j2smax[ies - 1] = j2s;
            j2tmax[ies - 1] = j2ts + 2 * (j2s - 1);
         }
      }  // iteration over eps1 eps2 that coupled to eps3
      // ies ... number of different eps2 such that eps1 + eps2 = eps3
      iesmax = ies;
      if (jj3td == jjtd) {  // if eps3 = eps3^0
         break;
      }
      // 2Lambda_3'  = 2Lambda_3
      j3tp = j3t;
      jj2tdp = jj2tdc;

      // copy content of dwu3 --> dwu3p
      indq = 0;
      // iterate over ieps2 = 0; ieps2 < iesmax
      for (int ies = 1; ies <= iesmax; ++ies) {
         // j2tmap[ieps2] = j2tmax[ieps2];
         j2tmap[ies - 1] = j2tmax[ies - 1];
         // j2sb --> number of different Lambda_2 associated with eps2
         int j2sb = j2smax[ies - 1];
         int j2sq = -n2;
         // iterate over j2s: iLambda_2 - 1
         for (int j2s = 1; j2s <= j2sb; ++j2s) {
            // j2sq = iLambda_2*n2 = (j2s - 1)*n2
            j2sq = j2sq + n2;
            // iesj2s - 1 = ieps2 + 1 + iLambda_2*n2 = [iLambda_2][ieps2]
            int iesj2s = ies + j2sq;
            indmap[iesj2s - 1] = indmat[iesj2s - 1];
            // j1sb = j1smax[iLambda2][ieps2] = number of different 2Lambda_1 associated with eps2
            // Lambda_2
            int j1sb = j1smax[iesj2s - 1];
            for (int j1s = 1; j1s <= j1sb; ++j1s) {
               for (int rho = 0; rho < rhomax; ++rho) {
                  dwu3p[indq] = dwu3[indq];
                  indq = indq + 1;
               }
            }
         }
      }
      std::fill(dwu3.begin(), dwu3.begin() + indq, 0);
   }  // jj3td: eps3 = eps_3HW ... eps3(input)
   indmax = ind;
   ie2max = -(lm2 + 2 * mu2) + 3 * (jj2tdc - 1) + 3 * (ies - 1);

   j2tmax.resize(iesmax);
   j2smax.resize(iesmax);
}


// Extreme SU(3)>SU(2)xU(1) coefficients are computed each time xwu3 is executed.
// For large-scale computations may be advisable to store extreme coefficients in fast look-up table
//
// std::vector<std::tuple<int, int, int>> = vector<e2, 2*LM2, 2*LM1>
template <typename T>
void xwu3_helper(int lm1, int mu1, int lm2, int mu2, int lm3, int mu3, int eps3, int LM3, int rhomax,
          std::vector<std::tuple<int, int, int>>& labels_e2LM2LM1, std::vector<double>& dwu3) {
   std::vector<int> j1t_cpp;
   std::vector<int> j2t_cpp;
   std::vector<int> ie2_cpp;

   assert(rhomax == su3::mult(lm1, mu1, lm2, mu2, lm3, mu3));

   const int I3 = 1;
   // nec will computed by xewu3, but it is not needed for our purposes
   int nec;

/*
   std::vector<double> dewu3_dbl;
   su3::xewu3_internal_to_dbl<T>(lm1, mu1, lm2, mu2, lm3, mu3, I3, nec, rhomax, j1t_cpp, j2t_cpp, ie2_cpp, dewu3_dbl);
*/
   std::vector<T> dewu3;
   // Compute {<lm1_ mu1_) * (lm2_ mu2_) * || (lm3_ mu3_) HW/HW'> coefficients}
   su3::xewu3_internal(lm1, mu1, lm2, mu2, lm3, mu3, I3, nec, rhomax, j1t_cpp, j2t_cpp, ie2_cpp, dewu3);

   std::vector<int> j1smax;
   std::vector<int> j1tmax;
   std::vector<int> j2tmax;
   std::vector<int> j2smax;
   std::vector<int> indmat;
   int indmax;
   int iesmax, eps2max;
   std::vector<T> dwu3T;
   su3::xwu3_internal(lm1, mu1, lm2, mu2, lm3, mu3, eps3, LM3, dewu3, rhomax, indmax, j1smax,
                         j1tmax, j2smax, j2tmax, eps2max, indmat, dwu3T);
   // dwu3T --> resulting vector<double> dwu3
   dwu3.resize(dwu3T.size());
   for (size_t i = 0; i < dwu3T.size(); ++i)
   {
      dwu3[i] = (double)dwu3T[i];
   }

   labels_e2LM2LM1.clear();
   // indmax == number of eps1 LM1 eps2 LM2 sets that couple to eps3 LM3
   labels_e2LM2LM1.reserve(indmax);
   std::tuple<int, int, int> eps2_LM2_LM1;
   int dim = lm2 + mu2 + 1;
   iesmax = j2tmax.size();
   for (int ies = 0; ies < iesmax; ++ies) {
      int eps2 = eps2max - 3 * (iesmax - (ies + 1));
      for (int j2s = 0; j2s < j2smax[ies]; ++j2s) {
         int LM2 = j2tmax[ies] - 2 * j2s;  // LM2 = 2*Lambda_2
         int iesj2s = ies + dim * j2s;
         for (int j1s = 0; j1s < j1smax[iesj2s]; ++j1s) {
            int LM1 = j1tmax[iesj2s] - 2 * j1s;  // LM1 = 2*Lambda_1
            labels_e2LM2LM1.push_back(std::make_tuple(eps2, LM2, LM1));
         }
      }
   }
}


}  // namespace su3
#endif
