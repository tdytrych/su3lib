#ifndef SU3_SU3_SO3_H
#define SU3_SU3_SO3_H

#include <vector>

namespace su3
{

// Generate {< (lm1 mu1) k1 L1; (lm2 mu2) k2 L2 || (lm3 mu3) k3 L3>_{rho}} == wu3r3w[k3][k2][k1][rho] 
//
// values computed for all possible values
// 0 <= k3 < k3max
// 0 <= k2 < k2max
// 0 <= k1 < k1max
// 0 <= rho < rhomax
void  wu3r3w(int lm1, int mu1, int lm2, int mu2, int lm3, int mu3, int L1, int L2, int L3, int k3max, int k2max, int k1max, int rhomax, std::vector<double>& su3cgs);
// CPC interface with inverse order of input multiplicities
void  wru3r3(int lm1, int mu1, int lm2, int mu2, int lm3, int mu3, int L1, int L2, int L3, int rhomax, int k1max, int k2max, int k3max, std::vector<double>& su3cgs);
// Generate {< (lm1 mu1) k1 L1; (lm2 mu2) k2 L2 || (lm3 mu3) k3 L3>_{rho}} == wu3r3w[k3][k2][k1][rho] 
// Simplified interface ... rhomax, k1max, k2max and k3max computed internally
void  wu3r3w(int lm1, int mu1, int lm2, int mu2, int lm3, int mu3, int L1, int L2, int L3, std::vector<double>& su3cgs);

} // namespace su3

#endif
