#ifndef SU3_SU3_UTILS_H
#define SU3_SU3_UTILS_H

#include <tuple>
#include <vector>

namespace su3
{
// simple utility functions of SU3lib
// (lm1 mu1) x (lm2 mu2) --> vector<rhomax, lm, mu>
void couple(int lm1, int mu1, int lm2, int mu2, std::vector<std::tuple<int, int, int>>& ir3);
int mult(int lm1, int mu1, int lm2, int mu2, int lm3, int mu3);
inline int kmax(int lm, int mu, int L) {
   return (std::max(0, (lm + mu + 2 - L) / 2) - std::max(0, (lm + 1 - L) / 2) -
           std::max(0, (mu + 1 - L) / 2));
}
inline int dim(int lm, int mu) { return (lm + 1) * (mu + 1) * (lm + mu + 2) / 2; }

// Functions needed for internal purposed of SU3lib
// 
// returns EPS, LM, M_LM of extremal state identified according to Table I. Draayer& Akyiama
std::tuple<int, int, int> GetExtremalStateLabels(int I, int J, int lm, int mu);
void setU3ExtremeState(int lm, int mu, int& I, int& J, int ifix = 0, int jfix = 1);
// returns Elliot's minum K value for a given L
int Kmin(int I, int J, int lm, int mu, int L);

// j1td: ~ eps1
// j1t: 2LAM1
// j2td: ~ eps2
// j2t: 2LAM1
// [eps1, lm1, 2LM1, eps2, lm2, 2LM2] index pointing to  <(lm1 mu1) eps1 LM1; (lm2 mu2) eps2 LM2||(lm3 mu3) G_{E}>
inline int INDEX(int j1td, int lm1, int j1t, int j2td, int lm2, int j2t) {
   return j2td * (j2td + 1) * (3 * j1td + j2td + 5) / 6 + (j1td + 1) * (lm2 + j2td - j2t) / 2 +
          (lm1 + j1td - j1t) / 2;
}

} // namespace su3

#endif
