# set if needed:
#BOOST_ROOT=
#WIGXJPF_DIR=

CXX = g++

INC += -I$(WIGXJPF_DIR)/inc
INC += -I$(BOOST_ROOT)/include

CXXFLAGS += -std=c++14
CXXFLAGS += $(INC)

# optimization / debugging options:
CXXFLAGS += -O3 
CXXFLAGS += -DNDEBUG 
CXXFLAGS += -DDTU3R3_CACHE 
#CXXFLAGS += -O0 
#CXXFLAGS += -g 
#CXXFLAGS += -fsanitize=address

LDFLAGS += -L$(WIGXJPF_DIR)/lib -lwigxjpf

# OpenMP if applicable:
#CXXFLAGS += -fopenmp
#LDFLAGS += -fopenmp

# long double 80-bit:
CXXFLAGS += -DHAVE_80BIT_LONG_DOUBLE

# float128 support:
#CXXFLAGS += -DHAVE_FLOAT128 -std=gnu++14
#LDFLAGS += -lquadmath
